import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CookieParserService } from './providers/services/cookie-parser/cookie-parser.service';
import { GlobalConfigService } from './providers/services/config/config.service';
import { Logger } from '@nestjs/common';
import { json, urlencoded } from 'body-parser';

const logger = new Logger('main.ts');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const cookieParser = new CookieParserService();
  const sizeLimit = '10mb';

  const globalConfigService: GlobalConfigService = app.get(GlobalConfigService);
  const port = globalConfigService.getPort();

  app.enableCors({
    origin: true,
    allowedHeaders: 'Authorization,Content-Type,Accept,provo_csrf,secret',
    credentials: true,
    preflightContinue: false,
    methods: 'GET,PATCH,PUT,POST,DELETE,OPTIONS',
    optionsSuccessStatus: 204,
  });

  app.use((req: any, res: any, next: any) => {
    cookieParser.parse(req, res);
    next();
  });

  app.use(json({ limit: sizeLimit }));
  app.use(urlencoded({ limit: sizeLimit, extended: true }));

  await app.listen(port);
  logger.debug(`Server started and listening on: 0.0.0.0:${port}/`);
}
bootstrap();
