import { Controller, Get, Post, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('wl')
  async sendWitelistWarning(@Query() query: any): Promise<any> {
    return await this.appService.sendWitelistWarning(query);
  }
}
