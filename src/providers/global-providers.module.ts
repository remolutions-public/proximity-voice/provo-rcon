import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ErrorService } from './services/error/error.service';
import { GlobalConfigService } from './services/config/config.service';
import { Logger } from '@nestjs/common';

const SERVICES = [
  Logger,
  ErrorService,
  ConfigService,
  GlobalConfigService,
];

const MODULES = [];

@Module({
  imports: [...MODULES],
  providers: [...SERVICES],
  exports: [...SERVICES],
})
export class GlobalProvidersModule {}
