export const envVars = {
  ENV: process.env.ENV || 'DEV',
  ENV_MODE: process.env.ENV || 'DEV',
  LOG_MODES: process.env.LOG_MODES,
  API_PORT: process.env.API_PORT || '3301',
  RCON_HOST: process.env.RCON_HOST,
  RCON_PORT: process.env.RCON_PORT,
  RCON_PASSWORD: process.env.RCON_PASSWORD,
};
