import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { GlobalProvidersModule } from './providers/global-providers.module';
import { Logger } from '@nestjs/common';

const MODULES = [
  GlobalProvidersModule,
  ConfigModule.forRoot({
    isGlobal: true,
    envFilePath: '.env',
  }),
];

const SERVICES = [AppService, Logger];

@Module({
  imports: [...MODULES],
  controllers: [AppController],
  providers: [...SERVICES],
})
export class AppModule {}
