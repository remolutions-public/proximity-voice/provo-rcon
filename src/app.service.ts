import { Injectable } from '@nestjs/common';
import { ErrorService } from './providers/services/error/error.service';
import { Rcon } from 'rcon-client';
import { GlobalConfigService } from './providers/services/config/config.service';
import { Logger } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor(
    private readonly globalConfigService: GlobalConfigService,
    private readonly logger: Logger,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  async sendWitelistWarning(query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      // https://www.npmjs.com/package/rcon-client

      const steamid = query.steamid;
      const time = query.time;

      const rcon = (await Rcon.connect({
        host: this.globalConfigService.getValue('RCON_HOST'),
        port: parseInt(this.globalConfigService.getValue('RCON_PORT')),
        password: this.globalConfigService.getValue('RCON_PASSWORD'),
      }).catch((err) => {
        this.logger.error(err);
        reject(err);
      })) as Rcon;

      if (!(rcon == undefined)) {
        const result = await rcon.send(
          `ScriptCommand DSRPWarnPlayerBeforeKick ${steamid} ${time}`,
        );

        rcon.end();

        resolve(result);
      }
    }).catch((err) => ErrorService.getInst().handle(err));
  }
}
